<?php
require_once './code.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S01: PHP Basics and Selection Control structure</title>
</head>
<body>
    <h1>Echoing Values</h1>
    <!-- Using single quote for the echo -->
    <p><?php echo 'Good day $name! Your given email is $email' ?></p>
    <!-- Using double quote for the echo -->
    <p><?php echo "Good day $name! Your given email is $email" ?></p>

    <p><?php echo PI ?></p>
    <h1>Data Types</h1>
    <h3><?php echo $state; ?></h3>
    <h3><?php echo $country; ?></h3>
    <h3><?php echo $address; ?></h3>

    <h3>Integer</h3>
    <p><?php echo $age; ?></p>
    <p><?php echo $headCount; ?></p>

    <h3>Floats</h3>
    <p><?php echo $grade; ?></p>
    <p><?php echo $distanceInKilometers; ?></p>

    <h3>Boolean</h3>
    <!-- Normal echoing of boolean variables will not make it visible to the webpage -->
    <p><?php echo var_dump($hasTravelAbroad); ?></p>
    <p><?php echo var_dump($haveSymptoms); ?></p>
    
    <h3>Null</h3>
    <!-- Normal echoing of boolean variables will not make it visible to the webpage -->
    <p><?php echo var_dump($spouse); ?></p>
    <p><?php echo var_dump($middleName); ?></p>

    <h3>Arrays</h3>
    <p><?php echo $grades[0]; ?></p>

    <h3>Objects</h3>
    <p><?php echo $gradesObj->firstGrading; ?></p>
    <p><?php echo $personObj->address->country; ?></p>
    <p><?php echo $personObj->contact[0]; ?></p>

    <h3>gettype()</h3>
    <p><?php echo gettype($state); ?></p>
    <p><?php echo gettype($age)?></p>
    <p><?php echo gettype($grade)?></p>
    <p><?php echo gettype($hasTravelAbroad)?></p>
    <p><?php echo gettype($spouse)?></p>
    <p><?php echo gettype($grades)?></p>
    <p><?php echo gettype($gradesObj)?></p>
    
    <h1>Operators</h1>
    <h3>Assignment operator</h3>
    <p>X: <?php echo $x; ?></p>
    <p>Y: <?php echo $y; ?></p>
    <p>Is Legal Age: <?php echo var_dump($isLegalAge); ?></p>
    <p>Is Registered: <?php echo var_dump($isRegistered); ?></p>

    <h3>Arithmetic Operators</h3>
    <p>Sum: <?php echo $x + $y; ?></p>
    <p>Difference: <?php echo $x - $y; ?></p>
    <p>Product: <?php echo $x * $y; ?></p>
    <p>Quotient: <?php echo $x / $y; ?></p>

    <h3>Equality Operators</h3>
    <p>Loose Equality: <?php echo var_dump($x == '1342.14') ?></p>
    <p>Strict Equality: <?php echo var_dump($x === '1342.14') ?></p>

    <h3>Inequality operator</h3>
    <p>Loose Inequality: <?php echo var_dump($x != "1342.14"); ?></p>
    <p>Strict Inequality: <?php echo var_dump($x !== "1342.14"); ?></p>
    <!-- Strict equality/inequality is preferred so that we can check both of the value and data type given -->

    <h3>Greater/Lesser operator</h3>
    <p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
    <p>Is Greater: <?php echo var_dump($x > $y); ?></p>
    <p>Is Lesser or Equal: <?php echo var_dump($x <= $y); ?></p>
    <p>Is Greater or Equal: <?php echo var_dump($x >= $y); ?></p>

    <h3>Logical Operators</h3>
    <!-- Logical Operators are used wether an expression or group of expressions are either true or false -->
    <p>OR operator: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
    <p>AND operator: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
    <p>NOT operator: <?php echo var_dump(!$isLegalAge); ?></p>

    <h1>Function</h1>
    <p><?php echo getFullName('John', 'D.', 'Smith'); ?></p>

    <h1>Selection Control Structure</h1>
    <h3>IF-Else if-Else statement</h3>
    <p><?php echo determineTyphoonIntensity(39); ?></p>

    <h2>Ternary Sample (Is underage?)</h2>
    <p>78: <?php echo var_dump(isUnderAge(78)); ?></p>
    <p>17: <?php echo var_dump(isUnderAge(17)); ?></p>
    
    <h3>Switch</h3>
    <p><?php echo determineComputerUser(4) ?></p>

    <h3>Try-Catch-Finally</h3>
    <p><?php echo greeting('12') ?></p>
</body>
</html>